﻿using ScottPlot;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BadHandCalculator
{
    public static class ChartCreator
    {
        /// <summary>
        /// Creates charts given the specified deck and game configuration
        /// </summary>
        public static string CreateCharts(DeckAndGameConfiguration config)
        {
            int maxNumberOfTurns = config.NumberOfCardsInDeck / 4;
            var allPossibleNumbersOfMisses = EnumerateAllPossibleNumbersOfMisses(config).ToList();
            var allPossibleNumbersOfMissesAsDoubles = allPossibleNumbersOfMisses.Select(x => (double)x).ToList();

            var pathForCharts = GetAndCreatePathForCharts();

            for (int numberOfTurns = 1; numberOfTurns <= maxNumberOfTurns; numberOfTurns++)
            {
                var allExpectedNumberOfBadHands = EnumerateAllExpectedNumberOfBadHands(config, numberOfTurns, allPossibleNumbersOfMisses);
                var allProbabilitiesOfAtLeastOneBadHand = EnumerateAllProbabilitiesOfAtLeastOneBadHand(config, numberOfTurns, allPossibleNumbersOfMisses);

                string expectationPlotPath = pathForCharts + $"Expected number - {config.NumberOfCardsInDeck} card deck - {numberOfTurns} turns - {config.ComparisonType} {config.NumberOfBadCardsForBadHand} misses.png";
                string expectationPlotTitle = $"Expected number bad hands ({Compare.ComparisonTypeToSign(config.ComparisonType)} {config.NumberOfBadCardsForBadHand} misses) drawn in {numberOfTurns} turns";
                PlotAndSaveChart(allPossibleNumbersOfMissesAsDoubles, allExpectedNumberOfBadHands, expectationPlotPath, expectationPlotTitle, config);

                string probabilityPlotPath = pathForCharts + $"Probability - {config.NumberOfCardsInDeck} card deck - {numberOfTurns} turns - {config.ComparisonType} {config.NumberOfBadCardsForBadHand} misses.png";
                string probabilityPlotTitle = $"Probability of drawing at least one bad hand ({Compare.ComparisonTypeToSign(config.ComparisonType)} {config.NumberOfBadCardsForBadHand} misses) in {numberOfTurns} turns";
                PlotAndSaveChart(allPossibleNumbersOfMissesAsDoubles, allProbabilitiesOfAtLeastOneBadHand, probabilityPlotPath, probabilityPlotTitle, config);
            }

            return pathForCharts;
        }


        /// <summary>
        /// Returns the path for saving charts into and creates it if necessary
        /// </summary>
        private static string GetAndCreatePathForCharts()
        {
            var programPath = Directory.GetCurrentDirectory();
            var directorySeparator = System.IO.Path.DirectorySeparatorChar.ToString();
            var pathForCharts = programPath + directorySeparator + "Charts" + directorySeparator;

            if (Directory.Exists(pathForCharts) == false)
                Directory.CreateDirectory(pathForCharts);

            return pathForCharts;
        }

        /// <summary>
        /// Plots and saves the chart to the given path
        /// </summary>
        private static void PlotAndSaveChart(List<double> xs, List<double> ys, string path, string plotTitle, DeckAndGameConfiguration config)
        {
            var plot = new ScottPlot.Plot();

            var scatterPlot = plot.Add.ScatterPoints(xs, ys);

            plot.Axes.Bottom.Label.Text = "Number of misses in deck";
            plot.Axes.Title.Label.Text = plotTitle;
            // Adds smaller lines between the main lines in the BG
            plot.Axes.Hairline(true);

            // Makes things a bit bigger and easier to read without having to zoom in. Also makes the scales nicer
            plot.ScaleFactor = 2;

            plot.Grid.MajorLineWidth = 4;
            plot.Grid.MajorLineColor = Colors.Black.WithOpacity(.3);

            plot.SavePng(path, (int)config.ChartWidth, (int)config.ChartHeight);
        }

        /// <summary>
        /// Enumerates all possible numbers of misses in a deck
        /// </summary>
        private static IEnumerable<int> EnumerateAllPossibleNumbersOfMisses(DeckAndGameConfiguration config)
        {
            return Enumerable.Range(0, config.NumberOfCardsInDeck + 1);
        }

        /// <summary>
        /// Enumerates all expected numbers of bad hands - assigning the expected value to each possible number of misses in deck
        /// </summary>
        private static List<double> EnumerateAllExpectedNumberOfBadHands(DeckAndGameConfiguration config, int numberOfTurns, List<int> AllPossibleNumbersOfMissesInDeck)
        {
            List<double> allExpectedNumbersOfBadHands = new();

            foreach (var numberOfMisses in AllPossibleNumbersOfMissesInDeck)
            {
                var startingDeck = new Deck(config.NumberOfCardsInDeck, numberOfMisses);
                var startingSearchSpaceState = new SearchSpaceState(startingDeck, numberOfTurns, config.ComparisonType, config.NumberOfBadCardsForBadHand, config.NumberOfCardsDrawnEachTurn);

                var expectedNumberOfBadHands = BadHandCalc.CalculateExpectedNumberOfBadHands(startingSearchSpaceState);
                allExpectedNumbersOfBadHands.Add(expectedNumberOfBadHands);
            }

            return allExpectedNumbersOfBadHands;
        }

        /// <summary>
        /// Enumerates all probabilities getting at least one bad hand in a game - assigning the probability to each possible number of misses in deck
        /// </summary>
        private static List<double> EnumerateAllProbabilitiesOfAtLeastOneBadHand(DeckAndGameConfiguration config, int numberOfTurns, List<int> AllPossibleNumbersOfMissesInDeck)
        {
            List<double> allProbabilitiesOfAtLeastOneBadHand = new();

            foreach (var numberOfMisses in AllPossibleNumbersOfMissesInDeck)
            {
                var startingDeck = new Deck(config.NumberOfCardsInDeck, numberOfMisses);
                var startingSearchSpaceState = new SearchSpaceState(startingDeck, numberOfTurns, config.ComparisonType, config.NumberOfBadCardsForBadHand, config.NumberOfCardsDrawnEachTurn);

                var probabilityOfAtLeastOneBadHand = BadHandCalc.CalculateProbabilityOfAtLeastOneBadHand(startingSearchSpaceState);
                allProbabilitiesOfAtLeastOneBadHand.Add(probabilityOfAtLeastOneBadHand);
            }

            return allProbabilitiesOfAtLeastOneBadHand;
        }

    }
}
