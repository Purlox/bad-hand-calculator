﻿using ScottPlot;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BadHandCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DeckAndGameConfiguration Config { get; set; } = new();


        public MainWindow()
        {
            var configLoadedFromFile = DeckAndGameConfiguration.LoadFromFile();
            if (configLoadedFromFile != null)
                Config = configLoadedFromFile;

            InitializeComponent();

            // The updates have to happen after all other initialization has happened or else there is a chance that those objects will be null when we try to update them
            UpdateNumberOfHitsLabel();
            UpdateInNGamesLabel();
        }

        protected override void OnClosed(EventArgs e)
        {
            Config.SaveToFile();

            base.OnClosed(e);
        }


        /// <summary>
        /// Function that fires when the user changes the number of cards in their deck
        /// </summary>
        private void NumberOfCardsInDeckChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                NumOfCardsInDeck.Text = Config.NumberOfCardsInDeck.ToString();
                return;
            }

            if (int.TryParse(NumOfCardsInDeck.Text, out int cardsInDeck) == false)
            {
                Info.Content = "You need to write a number there";
                return;
            }

            Config.NumberOfCardsInDeck = cardsInDeck;
            UpdateNumberOfHitsLabel();

            Info.Content = "Changed number of cards in deck";
        }

        /// <summary>
        /// Function that fires when the user changes the number of misses in their deck
        /// </summary>
        private void NumberOfMissesChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                NumOfMissesInDeck.Text = Config.NumberOfMissesInDeck.ToString();
                return;
            }

            if (int.TryParse(NumOfMissesInDeck.Text, out int missesInDeck) == false)
            {
                Info.Content = "You need to write a number there";
                return;
            }

            Config.NumberOfMissesInDeck = missesInDeck;
            UpdateNumberOfHitsLabel();

            Info.Content = "Changed number of misses in deck";
        }

        /// <summary>
        /// Updates the label showing the number of hits
        /// </summary>
        private void UpdateNumberOfHitsLabel()
        {
            if (NumOfHitsInDeck != null)
                NumOfHitsInDeck.Text = Config.NumberOfHitsInDeck.ToString();
        }


        /// <summary>
        /// Function that fires when the user changes how many cards should be drawn each turn
        /// </summary>
        private void NumberOfCardsDrawnEachTurnChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                NumOfCardsDrawnEachTurn.Text = Config.NumberOfCardsDrawnEachTurn.ToString();
                return;
            }

            if (int.TryParse(NumOfCardsDrawnEachTurn.Text, out int numberOfCardsDrawnEachTurn) == false)
            {
                Info.Content = "You need to write a number there";
                return;
            }

            Config.NumberOfCardsDrawnEachTurn = numberOfCardsDrawnEachTurn;

            Info.Content = "Changed number of cards drawn each turn";
        }

        /// <summary>
        /// Function that fires when the user changes the number of turns they expect in a game
        /// </summary>
        private void NumberOfTurnsChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                NumOfTurns.Text = Config.NumberOfTurns.ToString();
                return;
            }

            if (int.TryParse(NumOfTurns.Text, out int numberOfTurns) == false)
            {
                Info.Content = "You need to write a number there";
                return;
            }

            Config.NumberOfTurns = numberOfTurns;

            Info.Content = "Changed number of turns";
        }

        /// <summary>
        /// Function that fires when the user changes the number of games they expect to happen
        /// </summary>
        private void NumberOfGamesChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                NumOfGames.Text = Config.NumberOfGames.ToString();

                return;
            }

            if (uint.TryParse(NumOfGames.Text, out uint numberOfGames) == false)
            {
                Info.Content = "You need to write a non-negative number there";
                return;
            }

            Config.NumberOfGames = numberOfGames;

            Info.Content = "Changed number of games";

            UpdateInNGamesLabel();
        }

        /// <summary>
        /// Updates the label saying 'in X games:' to be accurate
        /// </summary>
        private void UpdateInNGamesLabel()
        {
            InNGamesLabel.Content = $"in {Config.NumberOfGames} games:";
        }

        /// <summary>
        /// Function that fires when the user changes their choice of ComparisonType
        /// </summary>
        private void ComparisonTypeChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                ComparisonTypeChoiceBox.SelectedIndex = (int)Config.ComparisonType;

                return;
            }

            Config.ComparisonType = (ComparisonType)ComparisonTypeChoiceBox.SelectedIndex;

            Info.Content = "Changed comparison type";
        }

        /// <summary>
        /// Function that fires when the user changes the number of bad cards required for bad hands
        /// </summary>
        private void NumberOfBadCardsForBadHandChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                NumberOfBadCardsForBadHand.Text = Config.NumberOfBadCardsForBadHand.ToString();

                return;
            }

            if (int.TryParse(NumberOfBadCardsForBadHand.Text, out int numberOfBadCardsForBadHand) == false)
            {
                Info.Content = "You need to write a number there";
                return;
            }

            Config.NumberOfBadCardsForBadHand = numberOfBadCardsForBadHand;

            Info.Content = "Changed number of bad cards required for a bad hand";
        }

        /// <summary>
        /// Function that fires when the user changes the desired chart width
        /// </summary>
        private void ChartWidthChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                ChartWidth.Text = Config.ChartWidth.ToString();

                return;
            }

            if (uint.TryParse(ChartWidth.Text, out uint chartWidth) == false)
            {
                Info.Content = "You need to write a non-negative number there";
                return;
            }

            Config.ChartWidth = chartWidth;

            Info.Content = "Changed the desired chart width";
        }

        /// <summary>
        /// Function that fires when the user changes the desired chart height
        /// </summary>
        private void ChartHeightChanged(object sender, TextChangedEventArgs e)
        {
            if (IsInitialized == false)
            {
                ChartHeight.Text = Config.ChartHeight.ToString();

                return;
            }

            if (uint.TryParse(ChartHeight.Text, out uint chartHeight) == false)
            {
                Info.Content = "You need to write a non-negative number there";
                return;
            }

            Config.ChartHeight = chartHeight;

            Info.Content = "Changed the desired chart height";
        }


        /// <summary>
        /// Function that fires when the user presses the calculate button, causing us to finally do something
        /// </summary>
        private void CalculateButtonClicked(object sender, RoutedEventArgs e)
        {
            Info.Content = "Calculating!";

            var startingSearchSpaceState = new SearchSpaceState(Config);

            var expectedNumberOfBadHands = BadHandCalc.CalculateExpectedNumberOfBadHands(startingSearchSpaceState);
            var probabilityOfAtLeastOneBadHand = BadHandCalc.CalculateProbabilityOfAtLeastOneBadHand(startingSearchSpaceState);

            ExpectedNumOfBadHands.Text = expectedNumberOfBadHands.ToString("F3");
            ProbabilityOfAtLeastOneBadHand.Text = $"{probabilityOfAtLeastOneBadHand * 100:F1} %";

            var expectedNumberOfBadHandsInNGames = BadHandCalc.CalculateExpectedNumberOfBadHandsInNGames(expectedNumberOfBadHands, Config.NumberOfGames);
            var probabilityOfAtLeastOneBadHandInNGames = BadHandCalc.CalculateProbabilityOfAtLeastOneBadHandInNGames(probabilityOfAtLeastOneBadHand, Config.NumberOfGames);

            ExpectedNumOfBadHandsInNGames.Text = expectedNumberOfBadHandsInNGames.ToString("F3");
            ProbabilityOfAtLeastOneBadHandInNGames.Text = $"{probabilityOfAtLeastOneBadHandInNGames * 100:F1} %";

            Info.Content = "Finished calculating!";
        }


        /// <summary>
        /// Creates charts of all possible combinations of number of misses in the deck and turns (keeps the deck size constant to not make it take forever while calculating unnecessary possibilities)
        /// </summary>
        private void CreateChartsButtonClicked(object sender, RoutedEventArgs e)
        {
            Info.Content = "Started creating charts!";

            var pathForCharts = ChartCreator.CreateCharts(Config);

            Info.Content = $"Finished making charts into {pathForCharts}";
        }
    }
}