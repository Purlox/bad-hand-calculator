﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BadHandCalculator
{
    public record Deck
    {
        public int CurrentDeckSize { get; set; }
        public int MissesRemainingInTheDeck { get; set; }

        public Deck(int deckSize, int missesInDeck)
        {
            CurrentDeckSize = deckSize;
            MissesRemainingInTheDeck = missesInDeck;
        }
    }
}
