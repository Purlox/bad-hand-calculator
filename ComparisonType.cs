﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BadHandCalculator
{
    /// <summary>
    /// The way the numbers will get compared
    /// </summary>
    public enum ComparisonType
    {
        [Description("At most")]
        At_most,

        [Description("Exactly")]
        Exactly,

        [Description("At least")]
        At_least,

        [Description("Anything but")]
        Anything_but
    }


    public static class Compare
    {
        /// <summary>
        /// Compares the two values according to the comparison type and returns true if the comparison is true
        /// </summary>
        public static bool CompareValues(int leftValue, ComparisonType type, int rightValue)
        {
            return type switch
            {
                ComparisonType.At_most
                    => leftValue <= rightValue,
                ComparisonType.Exactly
                    => leftValue == rightValue,
                ComparisonType.At_least
                    => leftValue >= rightValue,
                ComparisonType.Anything_but
                    => leftValue != rightValue,
                _
                    => throw new NotImplementedException()
            };
        }


        /// <summary>
        /// Turns comparison type into its corresponding sign.
        /// Here we use signs that non-programmers should be able to understand, since the signs will go to output
        /// </summary>
        public static string ComparisonTypeToSign(ComparisonType comparisonType)
        {
            return comparisonType switch
            {
                ComparisonType.At_most
                    => "<=",
                ComparisonType.Exactly
                    => "=",
                ComparisonType.At_least
                    => ">=",
                ComparisonType.Anything_but
                    => "=/=",
                _
                    => throw new NotImplementedException()
            };
        }
    }
}
