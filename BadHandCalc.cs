﻿using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BadHandCalculator
{
    /// <summary>
    /// Calculates the probability (or expected number) of bad hands.
    /// Bad hand is defined as a hand that has a certain number of bad cards in it as defined by the comparison type and the number of bad cards to compare to.
    /// E.g. if the ComparisonType is At_least and the number of bad cards for a bad hand is 3, then bad hand is defined as any hand with at least 3 bad cards in it.
    /// </summary>
    public static class BadHandCalc
    {
        /// <summary>
        /// Expected number of bad hand per each possible deck combination
        /// </summary>
        private static Dictionary<SearchSpaceState, double> ExpectedNumberOfBadHandsPerDeck { get; set; } = new();


        /// <summary>
        /// Calculates the expected number of bad hands given a starting number of cards in deck and misses in deck
        /// </summary>
        public static double CalculateExpectedNumberOfBadHands(SearchSpaceState currentState)
        {
            double expectedNumberOfBadHands = 0;

            if (currentState.NumberOfTurnsLeft <= 0)
                return 0;
            if (currentState.Deck.CurrentDeckSize <= 0)
                return 0;
            if (currentState.Deck.CurrentDeckSize < currentState.Deck.MissesRemainingInTheDeck) // Impossible state
                return 0;
            if (ExpectedNumberOfBadHandsPerDeck.TryGetValue(currentState, out expectedNumberOfBadHands))
                return expectedNumberOfBadHands;

            var probabilityDistributionForDeck = new Hypergeometric(currentState.Deck.CurrentDeckSize, currentState.Deck.MissesRemainingInTheDeck, currentState.NumberOfCardsDrawnEachTurn);

            int maxPossibleNumberOfMissesInHand = Math.Min(currentState.NumberOfCardsDrawnEachTurn, currentState.Deck.MissesRemainingInTheDeck);
            for (int missesInHand = 0; missesInHand <= maxPossibleNumberOfMissesInHand; missesInHand++)
            {
                int missesRemainingInDeck = currentState.Deck.MissesRemainingInTheDeck - missesInHand;
                int cardsRemainingInDeck = currentState.Deck.CurrentDeckSize - currentState.NumberOfCardsDrawnEachTurn;
                Deck newDeckState = new Deck(cardsRemainingInDeck, missesRemainingInDeck);
                int newNumberOfTurns = currentState.NumberOfTurnsLeft - 1;

                SearchSpaceState newState = currentState with { NumberOfTurnsLeft = newNumberOfTurns, Deck = newDeckState };

                double expectedNumberOfBadHandsInDeck = CalculateExpectedNumberOfBadHands(newState);

                if (Compare.CompareValues(missesInHand, currentState.ComparisonType, currentState.NumberOfBadCardsForBadHand))
                    expectedNumberOfBadHandsInDeck++;

                // If the expected number is 0, then we skip calculating the probability for this combination since 0 * any probability = 0
                if (expectedNumberOfBadHandsInDeck == 0)
                    continue;

                double probabilityOfDrawingThisHand = probabilityDistributionForDeck.Probability(missesInHand);

                expectedNumberOfBadHands += probabilityOfDrawingThisHand * expectedNumberOfBadHandsInDeck;
            }

            ExpectedNumberOfBadHandsPerDeck.Add(currentState, expectedNumberOfBadHands);

            return expectedNumberOfBadHands;
        }

        /// <summary>
        /// Calculates the expected number of bad hands in N games
        /// </summary>
        public static double CalculateExpectedNumberOfBadHandsInNGames(double expectedNumberOfBadHandsIn1Game, uint numberOfGames)
        {
            return expectedNumberOfBadHandsIn1Game * numberOfGames;
        }


        /// <summary>
        /// Probability of getting at least one bad hand per each possible deck combination
        /// </summary>
        private static Dictionary<SearchSpaceState, double> ProbabilityOfAtLeastOneBadHand { get; set; } = new();


        /// <summary>
        /// Calculates the probability of getting at least one bad hand given a starting number of cards in deck and misses in deck
        /// </summary>
        public static double CalculateProbabilityOfAtLeastOneBadHand(SearchSpaceState currentState)
        {
            double probabilityOfAtLeastOneBadHand = 0;

            if (currentState.NumberOfTurnsLeft <= 0)
                return 0;
            if (currentState.Deck.CurrentDeckSize <= 0)
                return 0;
            if (currentState.Deck.CurrentDeckSize < currentState.Deck.MissesRemainingInTheDeck) // Impossible state
                return 0;
            if (ProbabilityOfAtLeastOneBadHand.TryGetValue(currentState, out probabilityOfAtLeastOneBadHand))
                return probabilityOfAtLeastOneBadHand;

            var probabilityDistributionForDeck = new Hypergeometric(currentState.Deck.CurrentDeckSize, currentState.Deck.MissesRemainingInTheDeck, currentState.NumberOfCardsDrawnEachTurn);

            int maxPossibleNumberOfMissesInHand = Math.Min(currentState.NumberOfCardsDrawnEachTurn, currentState.Deck.MissesRemainingInTheDeck);
            for (int missesInHand = 0; missesInHand <= maxPossibleNumberOfMissesInHand; missesInHand++)
            {
                int missesRemainingInDeck = currentState.Deck.MissesRemainingInTheDeck - missesInHand;
                int cardsRemainingInDeck = currentState.Deck.CurrentDeckSize - currentState.NumberOfCardsDrawnEachTurn;
                Deck newDeckState = new Deck(cardsRemainingInDeck, missesRemainingInDeck);
                int newNumberOfTurns = currentState.NumberOfTurnsLeft - 1;

                SearchSpaceState newState = currentState with { Deck = newDeckState, NumberOfTurnsLeft = newNumberOfTurns };

                double probabilityOfAtLeastOneBadHandInDeck = Compare.CompareValues(missesInHand, currentState.ComparisonType, currentState.NumberOfBadCardsForBadHand)
                    ? 1
                    : CalculateProbabilityOfAtLeastOneBadHand(newState);

                // If the expected number is 0, then we skip calculating the probability for this combination since 0 * any probability = 0
                if (probabilityOfAtLeastOneBadHandInDeck == 0)
                    continue;

                double probabilityOfDrawingThisHand = probabilityDistributionForDeck.Probability(missesInHand);

                probabilityOfAtLeastOneBadHand += probabilityOfDrawingThisHand * probabilityOfAtLeastOneBadHandInDeck;
            }

            ProbabilityOfAtLeastOneBadHand.Add(currentState, probabilityOfAtLeastOneBadHand);

            return probabilityOfAtLeastOneBadHand;
        }

        /// <summary>
        /// Calculates the probability of getting at least one bad hand in N games
        /// </summary>
        public static double CalculateProbabilityOfAtLeastOneBadHandInNGames(double probabilityOfAtLeastOneBadHandIn1Game, uint numberOfGames)
        {
            double probabilityOfNoBadHandsIn1Game = 1 - probabilityOfAtLeastOneBadHandIn1Game;
            double probabilityOfNoBadHandsInNGames = Math.Pow(probabilityOfNoBadHandsIn1Game, numberOfGames);
            double probabilityOfAtLeastOneBadHandInNGames = 1 - probabilityOfNoBadHandsInNGames;

            return probabilityOfAtLeastOneBadHandInNGames;
        }
    }
}
