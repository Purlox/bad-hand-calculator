# Bad hand calculator

## Description

Here is a program calculating the probability and expected number of bad hands within a game of Flesh and Blood.

Bad hand is defined as being a hand containing a particular number of misses in it. This can mean e.g. hands with all 4 cards being misses or hands where there are at most 2 misses in the hand.

For the purposes of this program, a card being a hit means that it's some type of card you don't mind being in your hand. Whereas a card being a miss means it's some type of card that isn't necessarily bad, but you don't want to see too many of them in the same hand (e.g. non-attack actions or defense reactions or blues or nonblocks).

Besides calculating those things, it can also create charts that show the probabilities and expected number of bad hands given a fixed deck size, but variable number of misses in deck. You trigger this feature by using the Chartify button, which will then create the charts/plots in a subfolder under the executable named Charts.

## Limitations and assumptions made

To make the calculation doable at all, some parts of the game had to be simplified. Here are (hopefully all of) the assumptions the program makes:

- You draw exactly 4 cards each turn
- There is no other way to draw (besides drawing 4 cards a turn)
- All cards in your deck can be neatly placed into 2 categories - either it's a hit or a miss
- You can predict how many turns the game is going to last
- The game doesn't go into second cycle (but not much calculation can be done there anyway because second cycles are deterministic from the way you played your first cycle)

Assumptions made for simplicity of calculating multiple games:

- You have the same ratios of cards between games
- You play the same number of turns each game

Neither of those assumptions fully reflects reality of course, but you can get close to it by putting the ratios and number of turns you expect to play within those games.
