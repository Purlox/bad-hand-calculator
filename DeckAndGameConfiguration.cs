﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BadHandCalculator
{
    [Serializable]
    public class DeckAndGameConfiguration
    {
        public int NumberOfCardsInDeck { get; set; } = 60;
        public int NumberOfMissesInDeck { get; set; } = 20;
        public int NumberOfHitsInDeck => NumberOfCardsInDeck - NumberOfMissesInDeck;

        public int NumberOfTurns { get; set; } = 15;
        public uint NumberOfGames { get; set; } = 6;

        public ComparisonType ComparisonType { get; set; } = ComparisonType.Exactly;
        public int NumberOfBadCardsForBadHand { get; set; } = 4;
        public int NumberOfCardsDrawnEachTurn { get; set; } = 4;

        public uint ChartWidth { get; set; } = 1200;
        public uint ChartHeight { get; set; } = 1200;


        private const string SaveFileName = "LastUsedParams.txt";

        /// <summary>
        /// Loads last used deck and game configuration from a file
        /// </summary>
        public static DeckAndGameConfiguration? LoadFromFile()
        {
            try
            {
                if (File.Exists(SaveFileName) == false)
                    return null;

                using Stream fileStream = File.OpenRead(SaveFileName);
                var config = JsonSerializer.Deserialize<DeckAndGameConfiguration>(fileStream);

                return config;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Saves the last used deck and game configuration to a file
        /// </summary>
        /// <returns>Returns a value describing whether or not saving to file happened successfully</returns>
        public bool SaveToFile()
        {
            try
            {
                using Stream fileStream = File.OpenWrite(SaveFileName);
            
                JsonSerializer.Serialize(fileStream, this);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
