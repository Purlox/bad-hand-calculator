﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BadHandCalculator
{
    /// <summary>
    /// The state of the probability search space.
    /// 
    /// I couldn't think of a better name for the record, but I'm sure there is one
    /// </summary>
    public record SearchSpaceState(
        Deck Deck,
        
        int NumberOfTurnsLeft,

        ComparisonType ComparisonType,
        int NumberOfBadCardsForBadHand,
        int NumberOfCardsDrawnEachTurn
        )
    {
        public SearchSpaceState(DeckAndGameConfiguration config)
            : this(new Deck(config.NumberOfCardsInDeck, config.NumberOfMissesInDeck), config.NumberOfTurns, config.ComparisonType, config.NumberOfBadCardsForBadHand, config.NumberOfCardsDrawnEachTurn)
        {

        }
    }
}
